import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title: string = 'Gigital';

  scrollToArtists(): void {
    const firstItem = <HTMLElement>document.getElementsByClassName('artist-list__item')[0];
    const firstItemPosition = firstItem.offsetTop - 40; //20 is padding

    window.scrollTo({ left: 0, top: firstItemPosition, behavior: 'smooth' });
  }
}

export class Artist {
  name: string;
  genres: string[];
  priceRange: string;
  locations: string[];
  image: string;
}
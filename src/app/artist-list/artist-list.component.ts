import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/throttleTime';

import { Artist } from '../artist'; 
import { ArtistService } from '../artist.service';
import { environment } from '../../environments/environment';


@Component({
  selector: 'app-artist-list',
  templateUrl: './artist-list.component.html',
  styleUrls: ['./artist-list.component.scss']
})
export class ArtistListComponent implements OnInit {
  constructor(private artistService: ArtistService) { }
  
  artists: Artist[];
  pageNumber: number = 1;
  maxPageNumber: number = 4;
  imgUrl: string = environment.imgUrl;

  getArtists(): void {
    this.artistService.getArtists()
      .subscribe(artists => this.artists = artists);
  }

  loadMoreArtists(): void {
    this.pageNumber ++;
    this.artistService.getArtists(this.pageNumber)
      .subscribe(artists => this.artists.push.apply(this.artists, artists));
    console.log('Got artists alright');
    console.log(this.artists);
  }

  ngOnInit() {
    this.getArtists();

    // infinite scroll
    // TODO: add loading state and better handling of "maxPageNumber" which is hardcoded for now
    Observable.fromEvent(window, 'scroll')
      .throttleTime(200)
      .subscribe(e => {
        const scroll =  window.scrollY;

        const artistListItems = document.getElementsByClassName('artist-list__item');
        if (artistListItems.length > 0) {
          const bottomItem = <HTMLElement>artistListItems[artistListItems.length-1];

          const bottomItemHeight = bottomItem.clientHeight;
          const bottomItemScroll = bottomItem.offsetTop;

          if (bottomItemScroll - scroll <= 750 && this.pageNumber !== this.maxPageNumber) {
            this.loadMoreArtists();
          };
        };
      });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Artist } from './artist';

@Injectable()
export class ArtistService {

  constructor(private http: HttpClient) { }

  private artistsUrl = 'https://gigital-webapi-prod.azurewebsites.net/api/search/';

  getArtists(page = 1, size = 15): Observable<Artist[]> {
    return this.http.get<Artist[]>(`${this.artistsUrl}?pageNumber=${page}&rowsPage=${size}`);
  }
}